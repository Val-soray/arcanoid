// Fill out your copyright notice in the Description page of Project Settings.


#include "ArcanoidPawn.h"
#include "BlockSpawner.h"
#include "Camera.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "Ball.h"
#include <Kismet/KismetMathLibrary.h>
#include "Tank.h"
#include "Bonus.h"
#include "Killer.h"
#include "Speed.h"
#include "Components/DecalComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerController.h"
#include "Materials/Material.h"


// Sets default values
AArcanoidPawn::AArcanoidPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("CAMERA"));
	RootComponent = Camera;
	SetCameraLocationAndRotation();
	RacketSpeed = 25.f;
}

// Called when the game starts or when spawned
void AArcanoidPawn::BeginPlay()
{
	Super::BeginPlay();

	SpawnRacket();
	SpawnTank();
	SpawnArtillery();

	APlayerController* PC = Cast<APlayerController>(GetController());
	if (PC)
	{
		PC->bShowMouseCursor = true;
	}
}

// Called every frame
void AArcanoidPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	
	
	DecalMove();
}

// Called to bind functionality to input
void AArcanoidPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Horisotal", this, &AArcanoidPawn::HorisontalInputReaction);

	//PlayerInputComponent->BindAxis("Fire", this, &AArcanoidPawn::BonusBall);
}

void AArcanoidPawn::SetCameraLocationAndRotation()
{
	SetActorLocation(FVector(-400, 0, 730),false,nullptr, ETeleportType(true));
	SetActorRotation(FRotator(-65, 0, 0), ETeleportType(true));
}

void AArcanoidPawn::SpawnRacket()
{
	Racket = GetWorld()->SpawnActor<ACamera>(RacketClass, FTransform(FVector(-400,0,50)));
	Racket->RacketOwner = this;
}



void AArcanoidPawn::HorisontalInputReaction(float value)
{
	FVector MoveDirection(RacketSpeed*Direction);
	FHitResult Hit;
	if (value > 0)
	{
		Direction = FVector(0, -1, 0);
		Direction = FVector(FMath::GetReflectionVector(Direction, Hit.Normal));
		Racket->AddActorWorldOffset(MoveDirection, true, &Hit);
	}
	if (value < 0)
	{
		Direction = FVector(0, 1, 0);
		Direction = FVector(FMath::GetReflectionVector(Direction, Hit.Normal));
		Racket->AddActorWorldOffset(MoveDirection,true, &Hit);
	}
}

void AArcanoidPawn::SpawnTank()
{
	FVector Location(x, y, z);
	for (int j = 0; j < 2; j++)
	{
		if (j == 1)
		{
			Location.X = Location.X + 3000;
			Location.Y = -6065.00;
		}
		for (int i = 0; i < 14; i++)
		{
			Tank = Cast<ATank>(GetWorld()->SpawnActor(TankClass, &Location));
			Tank->TankOwner = this;
			if(j == 0)
			{
				AllyTankArray.Add(Tank);
			}
			else
			{
				EnemyTankArray.Add(Tank);
				Tank->SetActorRotation(FRotator(0, 180, 0));
			}
			Location.Y = Location.Y + 400;
		}
	}
}

void AArcanoidPawn::DecalMove()
{
	APlayerController* PC = Cast<APlayerController>(GetController());
	if (PC)
	{
		FHitResult HitResult;
		PC->GetHitResultUnderCursor(ECC_Visibility, false, HitResult);
		FVector HitLocation = HitResult.Location;
		if(Decal)
		{
			Decal->SetWorldLocation(HitLocation);
		}
	}
}

void AArcanoidPawn::SpawnArtillery()
{
	FVector Location(x, y, z);

	Location.X = Location.X - 600;
	for (int j = 0; j < 2; j++)
	{
		Location.Y = -5865;
		if (j == 1)
		{
			Location.X = Location.X + 4200;
		}
		for (int i = 0; i < 7; i++)
		{
			Tank = Cast<ATank>(GetWorld()->SpawnActor(ArtilleryClass, &Location));
			if (j == 0)
			{
				AllyArtilleryArray.Add(Tank);
			}
			else
			{
				EnemyArtilleryArray.Add(Tank);
				Tank->SetActorRotation(FRotator(0, 180, 0));
			}
			Location.Y = Location.Y + 800;
		}
	}
}
