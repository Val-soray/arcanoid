// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball.h"
#include "ArcanoidPawn.h"
#include <Kismet/KismetMathLibrary.h>
#include "Speed.h"

// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Block);

	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABall::OverlapReaction);
	MeshComponent->OnComponentHit.AddDynamic(this, &ABall::HitReaction);
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();

	Direction = FVector(1, 1, 0);
	Sum = 0;
	SumToKill = 0;
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	BallMove(DeltaTime);

}

void ABall::BallMove(float DeltaTime)
{
	FVector SpeedDirection(Speed* DeltaTime * Direction);

	FHitResult OutSweepHitResult = FHitResult();

	AddActorWorldOffset(SpeedDirection, true, &OutSweepHitResult);
	FVector FullDirection = FVector(FMath::GetReflectionVector(Direction, OutSweepHitResult.Normal));
	Direction = FVector(FullDirection.X, FullDirection.Y, 0);
	MoveTimer = 1.0f;
}

void ABall::OverlapReaction(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
}

void ABall::HitReaction(UPrimitiveComponent* HitComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
}
