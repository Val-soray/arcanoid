// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "ArcanoidPawn.generated.h"

class ABlockSpawner;
class ATank;
class UCameraComponent;
class ACamera;
class ABall;
class ABonus;
class AKiller;
class ASpeed;

UCLASS()
class MYPROJECT11_API AArcanoidPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AArcanoidPawn();

	UPROPERTY()
		UCameraComponent* Camera;

	UPROPERTY()
		ACamera* Racket;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ACamera>RacketClass;

	UPROPERTY()
		ATank* Tank;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ATank> TankClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ATank> ArtilleryClass;

	UPROPERTY(BlueprintReadWrite)
		TArray <ATank*>AllyTankArray;

	UPROPERTY(BlueprintReadWrite)
		TArray <ATank*>EnemyTankArray;

	UPROPERTY(BlueprintReadWrite)
		TArray <ATank*>AllyArtilleryArray;

	UPROPERTY(BlueprintReadWrite)
		TArray <ATank*>EnemyArtilleryArray;

	UPROPERTY()
		ABall* Ball;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABall>BallClass;

	UPROPERTY(BlueprintReadWrite)
		TArray<ABall*>BallArray;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* DecalMaterial = nullptr;

	UDecalComponent* Decal = nullptr;

	bool bIsSpawnDecal = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MouseInterface);
	uint32 bShowMouseCursor : 1;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void SetCameraLocationAndRotation();

	UFUNCTION()
		void HorisontalInputReaction(float value);

	float RacketSpeed;
	FVector Direction;

	float x = -5100.000000;
	float y = -6065.00;
	float z = 100.000000;

	UFUNCTION()
		void SpawnRacket();

	UFUNCTION()
		void SpawnTank();

	UFUNCTION(BlueprintCallable)
		void DecalMove();

	UFUNCTION()
		void SpawnArtillery();
};
