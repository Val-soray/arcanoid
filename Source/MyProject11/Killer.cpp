// Fill out your copyright notice in the Description page of Project Settings.


#include "Killer.h"
#include "ArcanoidPawn.h"
#include "Ball.h"

// Sets default values
AKiller::AKiller()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AKiller::Kill);
}

// Called when the game starts or when spawned
void AKiller::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKiller::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKiller::Kill(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	if (OtherActor == Ball)
	{
		for (int i = 0; i < KillerOwner->BallArray.Num(); i++)
		{
			if (OtherActor == KillerOwner->BallArray[i])
			{
				OtherActor->Destroy();
			}
		}
	}
}
