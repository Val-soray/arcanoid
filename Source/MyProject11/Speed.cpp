// Fill out your copyright notice in the Description page of Project Settings.


#include "Speed.h"
#include "ArcanoidPawn.h"

// Sets default values
ASpeed::ASpeed()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASpeed::Speed);
}

// Called when the game starts or when spawned
void ASpeed::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeed::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpeed::Speed(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
}

