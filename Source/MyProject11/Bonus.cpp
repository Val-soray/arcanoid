// Fill out your copyright notice in the Description page of Project Settings.


#include "Bonus.h"
#include "Camera.h"
#include "ArcanoidPawn.h"

// Sets default values
ABonus::ABonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABonus::BonusOverlap);
}

// Called when the game starts or when spawned
void ABonus::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ABonus::Move()
{
	AddActorWorldOffset(FVector(-3, 0, 0));
}

void ABonus::BonusOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	if(IsValid(BonusOwner))
	{
		//BonusOwner->BonusBall(OtherActor, this);
	}
}
