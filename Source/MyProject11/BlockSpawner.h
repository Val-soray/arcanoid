// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlockSpawner.generated.h"

class ATank;
class AArcanoidPawn;

UCLASS()
class MYPROJECT11_API ABlockSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlockSpawner();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf <ATank> BlockClass;

	UPROPERTY()
		TArray<ATank*>BlockArray;

	UPROPERTY()
		AArcanoidPawn* BlockSpawnerSpawner;

	UPROPERTY()
		ATank* Block;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void SpawnBlocks(int I,int J,FVector Location);

};
