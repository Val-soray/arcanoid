// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT11_MyProject11GameModeBase_generated_h
#error "MyProject11GameModeBase.generated.h already included, missing '#pragma once' in MyProject11GameModeBase.h"
#endif
#define MYPROJECT11_MyProject11GameModeBase_generated_h

#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_SPARSE_DATA
#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_RPC_WRAPPERS
#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyProject11GameModeBase(); \
	friend struct Z_Construct_UClass_AMyProject11GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMyProject11GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(AMyProject11GameModeBase)


#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMyProject11GameModeBase(); \
	friend struct Z_Construct_UClass_AMyProject11GameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMyProject11GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(AMyProject11GameModeBase)


#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyProject11GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyProject11GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyProject11GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyProject11GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyProject11GameModeBase(AMyProject11GameModeBase&&); \
	NO_API AMyProject11GameModeBase(const AMyProject11GameModeBase&); \
public:


#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyProject11GameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyProject11GameModeBase(AMyProject11GameModeBase&&); \
	NO_API AMyProject11GameModeBase(const AMyProject11GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyProject11GameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyProject11GameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyProject11GameModeBase)


#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_12_PROLOG
#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_SPARSE_DATA \
	MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_RPC_WRAPPERS \
	MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_INCLASS \
	MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_SPARSE_DATA \
	MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_MyProject11GameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT11_API UClass* StaticClass<class AMyProject11GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject11_Source_MyProject11_MyProject11GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
