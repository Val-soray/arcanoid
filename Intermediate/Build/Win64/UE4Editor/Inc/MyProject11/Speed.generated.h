// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef MYPROJECT11_Speed_generated_h
#error "Speed.generated.h already included, missing '#pragma once' in Speed.h"
#endif
#define MYPROJECT11_Speed_generated_h

#define MyProject11_Source_MyProject11_Speed_h_14_SPARSE_DATA
#define MyProject11_Source_MyProject11_Speed_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpeed);


#define MyProject11_Source_MyProject11_Speed_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpeed);


#define MyProject11_Source_MyProject11_Speed_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpeed(); \
	friend struct Z_Construct_UClass_ASpeed_Statics; \
public: \
	DECLARE_CLASS(ASpeed, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(ASpeed)


#define MyProject11_Source_MyProject11_Speed_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASpeed(); \
	friend struct Z_Construct_UClass_ASpeed_Statics; \
public: \
	DECLARE_CLASS(ASpeed, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(ASpeed)


#define MyProject11_Source_MyProject11_Speed_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpeed(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpeed) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeed); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeed); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeed(ASpeed&&); \
	NO_API ASpeed(const ASpeed&); \
public:


#define MyProject11_Source_MyProject11_Speed_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpeed(ASpeed&&); \
	NO_API ASpeed(const ASpeed&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpeed); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpeed); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpeed)


#define MyProject11_Source_MyProject11_Speed_h_14_PRIVATE_PROPERTY_OFFSET
#define MyProject11_Source_MyProject11_Speed_h_11_PROLOG
#define MyProject11_Source_MyProject11_Speed_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_Speed_h_14_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_Speed_h_14_SPARSE_DATA \
	MyProject11_Source_MyProject11_Speed_h_14_RPC_WRAPPERS \
	MyProject11_Source_MyProject11_Speed_h_14_INCLASS \
	MyProject11_Source_MyProject11_Speed_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject11_Source_MyProject11_Speed_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_Speed_h_14_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_Speed_h_14_SPARSE_DATA \
	MyProject11_Source_MyProject11_Speed_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_Speed_h_14_INCLASS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_Speed_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT11_API UClass* StaticClass<class ASpeed>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject11_Source_MyProject11_Speed_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
