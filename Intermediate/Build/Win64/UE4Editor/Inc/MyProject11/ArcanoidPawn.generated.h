// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT11_ArcanoidPawn_generated_h
#error "ArcanoidPawn.generated.h already included, missing '#pragma once' in ArcanoidPawn.h"
#endif
#define MYPROJECT11_ArcanoidPawn_generated_h

#define MyProject11_Source_MyProject11_ArcanoidPawn_h_21_SPARSE_DATA
#define MyProject11_Source_MyProject11_ArcanoidPawn_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnArtillery); \
	DECLARE_FUNCTION(execDecalMove); \
	DECLARE_FUNCTION(execSpawnTank); \
	DECLARE_FUNCTION(execSpawnRacket); \
	DECLARE_FUNCTION(execHorisontalInputReaction); \
	DECLARE_FUNCTION(execSetCameraLocationAndRotation);


#define MyProject11_Source_MyProject11_ArcanoidPawn_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnArtillery); \
	DECLARE_FUNCTION(execDecalMove); \
	DECLARE_FUNCTION(execSpawnTank); \
	DECLARE_FUNCTION(execSpawnRacket); \
	DECLARE_FUNCTION(execHorisontalInputReaction); \
	DECLARE_FUNCTION(execSetCameraLocationAndRotation);


#define MyProject11_Source_MyProject11_ArcanoidPawn_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArcanoidPawn(); \
	friend struct Z_Construct_UClass_AArcanoidPawn_Statics; \
public: \
	DECLARE_CLASS(AArcanoidPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(AArcanoidPawn)


#define MyProject11_Source_MyProject11_ArcanoidPawn_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAArcanoidPawn(); \
	friend struct Z_Construct_UClass_AArcanoidPawn_Statics; \
public: \
	DECLARE_CLASS(AArcanoidPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(AArcanoidPawn)


#define MyProject11_Source_MyProject11_ArcanoidPawn_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArcanoidPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArcanoidPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArcanoidPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArcanoidPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArcanoidPawn(AArcanoidPawn&&); \
	NO_API AArcanoidPawn(const AArcanoidPawn&); \
public:


#define MyProject11_Source_MyProject11_ArcanoidPawn_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArcanoidPawn(AArcanoidPawn&&); \
	NO_API AArcanoidPawn(const AArcanoidPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArcanoidPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArcanoidPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AArcanoidPawn)


#define MyProject11_Source_MyProject11_ArcanoidPawn_h_21_PRIVATE_PROPERTY_OFFSET
#define MyProject11_Source_MyProject11_ArcanoidPawn_h_18_PROLOG
#define MyProject11_Source_MyProject11_ArcanoidPawn_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_ArcanoidPawn_h_21_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_ArcanoidPawn_h_21_SPARSE_DATA \
	MyProject11_Source_MyProject11_ArcanoidPawn_h_21_RPC_WRAPPERS \
	MyProject11_Source_MyProject11_ArcanoidPawn_h_21_INCLASS \
	MyProject11_Source_MyProject11_ArcanoidPawn_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject11_Source_MyProject11_ArcanoidPawn_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_ArcanoidPawn_h_21_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_ArcanoidPawn_h_21_SPARSE_DATA \
	MyProject11_Source_MyProject11_ArcanoidPawn_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_ArcanoidPawn_h_21_INCLASS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_ArcanoidPawn_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT11_API UClass* StaticClass<class AArcanoidPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject11_Source_MyProject11_ArcanoidPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
