// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
#ifdef MYPROJECT11_BlockSpawner_generated_h
#error "BlockSpawner.generated.h already included, missing '#pragma once' in BlockSpawner.h"
#endif
#define MYPROJECT11_BlockSpawner_generated_h

#define MyProject11_Source_MyProject11_BlockSpawner_h_15_SPARSE_DATA
#define MyProject11_Source_MyProject11_BlockSpawner_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnBlocks);


#define MyProject11_Source_MyProject11_BlockSpawner_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnBlocks);


#define MyProject11_Source_MyProject11_BlockSpawner_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABlockSpawner(); \
	friend struct Z_Construct_UClass_ABlockSpawner_Statics; \
public: \
	DECLARE_CLASS(ABlockSpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(ABlockSpawner)


#define MyProject11_Source_MyProject11_BlockSpawner_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABlockSpawner(); \
	friend struct Z_Construct_UClass_ABlockSpawner_Statics; \
public: \
	DECLARE_CLASS(ABlockSpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(ABlockSpawner)


#define MyProject11_Source_MyProject11_BlockSpawner_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABlockSpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABlockSpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABlockSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABlockSpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABlockSpawner(ABlockSpawner&&); \
	NO_API ABlockSpawner(const ABlockSpawner&); \
public:


#define MyProject11_Source_MyProject11_BlockSpawner_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABlockSpawner(ABlockSpawner&&); \
	NO_API ABlockSpawner(const ABlockSpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABlockSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABlockSpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABlockSpawner)


#define MyProject11_Source_MyProject11_BlockSpawner_h_15_PRIVATE_PROPERTY_OFFSET
#define MyProject11_Source_MyProject11_BlockSpawner_h_12_PROLOG
#define MyProject11_Source_MyProject11_BlockSpawner_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_BlockSpawner_h_15_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_BlockSpawner_h_15_SPARSE_DATA \
	MyProject11_Source_MyProject11_BlockSpawner_h_15_RPC_WRAPPERS \
	MyProject11_Source_MyProject11_BlockSpawner_h_15_INCLASS \
	MyProject11_Source_MyProject11_BlockSpawner_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject11_Source_MyProject11_BlockSpawner_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_BlockSpawner_h_15_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_BlockSpawner_h_15_SPARSE_DATA \
	MyProject11_Source_MyProject11_BlockSpawner_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_BlockSpawner_h_15_INCLASS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_BlockSpawner_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT11_API UClass* StaticClass<class ABlockSpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject11_Source_MyProject11_BlockSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
