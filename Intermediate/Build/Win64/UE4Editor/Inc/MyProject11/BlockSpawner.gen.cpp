// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyProject11/BlockSpawner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBlockSpawner() {}
// Cross Module References
	MYPROJECT11_API UClass* Z_Construct_UClass_ABlockSpawner_NoRegister();
	MYPROJECT11_API UClass* Z_Construct_UClass_ABlockSpawner();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_MyProject11();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	MYPROJECT11_API UClass* Z_Construct_UClass_ATank_NoRegister();
	MYPROJECT11_API UClass* Z_Construct_UClass_AArcanoidPawn_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ABlockSpawner::execSpawnBlocks)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_I);
		P_GET_PROPERTY(FIntProperty,Z_Param_J);
		P_GET_STRUCT(FVector,Z_Param_Location);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnBlocks(Z_Param_I,Z_Param_J,Z_Param_Location);
		P_NATIVE_END;
	}
	void ABlockSpawner::StaticRegisterNativesABlockSpawner()
	{
		UClass* Class = ABlockSpawner::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SpawnBlocks", &ABlockSpawner::execSpawnBlocks },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics
	{
		struct BlockSpawner_eventSpawnBlocks_Parms
		{
			int32 I;
			int32 J;
			FVector Location;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_I;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_J;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::NewProp_I = { "I", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BlockSpawner_eventSpawnBlocks_Parms, I), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::NewProp_J = { "J", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BlockSpawner_eventSpawnBlocks_Parms, J), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BlockSpawner_eventSpawnBlocks_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::NewProp_I,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::NewProp_J,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::NewProp_Location,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BlockSpawner.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABlockSpawner, nullptr, "SpawnBlocks", nullptr, nullptr, sizeof(BlockSpawner_eventSpawnBlocks_Parms), Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABlockSpawner_SpawnBlocks()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABlockSpawner_SpawnBlocks_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABlockSpawner_NoRegister()
	{
		return ABlockSpawner::StaticClass();
	}
	struct Z_Construct_UClass_ABlockSpawner_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlockClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_BlockClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BlockArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlockArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BlockArray;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlockSpawnerSpawner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BlockSpawnerSpawner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Block_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Block;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABlockSpawner_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_MyProject11,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABlockSpawner_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABlockSpawner_SpawnBlocks, "SpawnBlocks" }, // 3017978936
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlockSpawner_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BlockSpawner.h" },
		{ "ModuleRelativePath", "BlockSpawner.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockClass_MetaData[] = {
		{ "Category", "BlockSpawner" },
		{ "ModuleRelativePath", "BlockSpawner.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockClass = { "BlockClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABlockSpawner, BlockClass), Z_Construct_UClass_ATank_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockArray_Inner = { "BlockArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ATank_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockArray_MetaData[] = {
		{ "ModuleRelativePath", "BlockSpawner.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockArray = { "BlockArray", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABlockSpawner, BlockArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockArray_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockSpawnerSpawner_MetaData[] = {
		{ "ModuleRelativePath", "BlockSpawner.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockSpawnerSpawner = { "BlockSpawnerSpawner", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABlockSpawner, BlockSpawnerSpawner), Z_Construct_UClass_AArcanoidPawn_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockSpawnerSpawner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockSpawnerSpawner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABlockSpawner_Statics::NewProp_Block_MetaData[] = {
		{ "ModuleRelativePath", "BlockSpawner.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABlockSpawner_Statics::NewProp_Block = { "Block", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABlockSpawner, Block), Z_Construct_UClass_ATank_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABlockSpawner_Statics::NewProp_Block_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABlockSpawner_Statics::NewProp_Block_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABlockSpawner_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlockSpawner_Statics::NewProp_BlockSpawnerSpawner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABlockSpawner_Statics::NewProp_Block,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABlockSpawner_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABlockSpawner>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABlockSpawner_Statics::ClassParams = {
		&ABlockSpawner::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABlockSpawner_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABlockSpawner_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABlockSpawner_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABlockSpawner_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABlockSpawner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABlockSpawner_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABlockSpawner, 2048263834);
	template<> MYPROJECT11_API UClass* StaticClass<ABlockSpawner>()
	{
		return ABlockSpawner::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABlockSpawner(Z_Construct_UClass_ABlockSpawner, &ABlockSpawner::StaticClass, TEXT("/Script/MyProject11"), TEXT("ABlockSpawner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABlockSpawner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
