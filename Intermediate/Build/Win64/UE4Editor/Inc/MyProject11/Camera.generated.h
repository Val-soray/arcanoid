// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef MYPROJECT11_Camera_generated_h
#error "Camera.generated.h already included, missing '#pragma once' in Camera.h"
#endif
#define MYPROJECT11_Camera_generated_h

#define MyProject11_Source_MyProject11_Camera_h_14_SPARSE_DATA
#define MyProject11_Source_MyProject11_Camera_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHitReaction);


#define MyProject11_Source_MyProject11_Camera_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHitReaction);


#define MyProject11_Source_MyProject11_Camera_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACamera(); \
	friend struct Z_Construct_UClass_ACamera_Statics; \
public: \
	DECLARE_CLASS(ACamera, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(ACamera)


#define MyProject11_Source_MyProject11_Camera_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACamera(); \
	friend struct Z_Construct_UClass_ACamera_Statics; \
public: \
	DECLARE_CLASS(ACamera, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(ACamera)


#define MyProject11_Source_MyProject11_Camera_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACamera(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACamera) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACamera); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACamera); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACamera(ACamera&&); \
	NO_API ACamera(const ACamera&); \
public:


#define MyProject11_Source_MyProject11_Camera_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACamera(ACamera&&); \
	NO_API ACamera(const ACamera&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACamera); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACamera); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACamera)


#define MyProject11_Source_MyProject11_Camera_h_14_PRIVATE_PROPERTY_OFFSET
#define MyProject11_Source_MyProject11_Camera_h_11_PROLOG
#define MyProject11_Source_MyProject11_Camera_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_Camera_h_14_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_Camera_h_14_SPARSE_DATA \
	MyProject11_Source_MyProject11_Camera_h_14_RPC_WRAPPERS \
	MyProject11_Source_MyProject11_Camera_h_14_INCLASS \
	MyProject11_Source_MyProject11_Camera_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject11_Source_MyProject11_Camera_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_Camera_h_14_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_Camera_h_14_SPARSE_DATA \
	MyProject11_Source_MyProject11_Camera_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_Camera_h_14_INCLASS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_Camera_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT11_API UClass* StaticClass<class ACamera>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject11_Source_MyProject11_Camera_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
