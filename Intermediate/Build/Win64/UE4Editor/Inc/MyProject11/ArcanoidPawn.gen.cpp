// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MyProject11/ArcanoidPawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeArcanoidPawn() {}
// Cross Module References
	MYPROJECT11_API UClass* Z_Construct_UClass_AArcanoidPawn_NoRegister();
	MYPROJECT11_API UClass* Z_Construct_UClass_AArcanoidPawn();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_MyProject11();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	MYPROJECT11_API UClass* Z_Construct_UClass_ACamera_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	MYPROJECT11_API UClass* Z_Construct_UClass_ATank_NoRegister();
	MYPROJECT11_API UClass* Z_Construct_UClass_ABall_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AArcanoidPawn::execSpawnArtillery)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnArtillery();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcanoidPawn::execDecalMove)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DecalMove();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcanoidPawn::execSpawnTank)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnTank();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcanoidPawn::execSpawnRacket)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SpawnRacket();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcanoidPawn::execHorisontalInputReaction)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HorisontalInputReaction(Z_Param_value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AArcanoidPawn::execSetCameraLocationAndRotation)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetCameraLocationAndRotation();
		P_NATIVE_END;
	}
	void AArcanoidPawn::StaticRegisterNativesAArcanoidPawn()
	{
		UClass* Class = AArcanoidPawn::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DecalMove", &AArcanoidPawn::execDecalMove },
			{ "HorisontalInputReaction", &AArcanoidPawn::execHorisontalInputReaction },
			{ "SetCameraLocationAndRotation", &AArcanoidPawn::execSetCameraLocationAndRotation },
			{ "SpawnArtillery", &AArcanoidPawn::execSpawnArtillery },
			{ "SpawnRacket", &AArcanoidPawn::execSpawnRacket },
			{ "SpawnTank", &AArcanoidPawn::execSpawnTank },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AArcanoidPawn_DecalMove_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidPawn_DecalMove_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidPawn_DecalMove_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidPawn, nullptr, "DecalMove", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidPawn_DecalMove_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidPawn_DecalMove_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidPawn_DecalMove()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidPawn_DecalMove_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics
	{
		struct ArcanoidPawn_eventHorisontalInputReaction_Parms
		{
			float value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics::NewProp_value = { "value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ArcanoidPawn_eventHorisontalInputReaction_Parms, value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics::NewProp_value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidPawn, nullptr, "HorisontalInputReaction", nullptr, nullptr, sizeof(ArcanoidPawn_eventHorisontalInputReaction_Parms), Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcanoidPawn_SetCameraLocationAndRotation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidPawn_SetCameraLocationAndRotation_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidPawn_SetCameraLocationAndRotation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidPawn, nullptr, "SetCameraLocationAndRotation", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidPawn_SetCameraLocationAndRotation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidPawn_SetCameraLocationAndRotation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidPawn_SetCameraLocationAndRotation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidPawn_SetCameraLocationAndRotation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcanoidPawn_SpawnArtillery_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidPawn_SpawnArtillery_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidPawn_SpawnArtillery_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidPawn, nullptr, "SpawnArtillery", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidPawn_SpawnArtillery_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidPawn_SpawnArtillery_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidPawn_SpawnArtillery()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidPawn_SpawnArtillery_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcanoidPawn_SpawnRacket_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidPawn_SpawnRacket_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidPawn_SpawnRacket_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidPawn, nullptr, "SpawnRacket", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidPawn_SpawnRacket_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidPawn_SpawnRacket_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidPawn_SpawnRacket()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidPawn_SpawnRacket_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AArcanoidPawn_SpawnTank_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AArcanoidPawn_SpawnTank_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AArcanoidPawn_SpawnTank_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AArcanoidPawn, nullptr, "SpawnTank", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AArcanoidPawn_SpawnTank_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AArcanoidPawn_SpawnTank_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AArcanoidPawn_SpawnTank()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AArcanoidPawn_SpawnTank_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AArcanoidPawn_NoRegister()
	{
		return AArcanoidPawn::StaticClass();
	}
	struct Z_Construct_UClass_AArcanoidPawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Camera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Racket_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Racket;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RacketClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_RacketClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tank_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Tank;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TankClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_TankClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArtilleryClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ArtilleryClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AllyTankArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllyTankArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllyTankArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EnemyTankArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyTankArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnemyTankArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AllyArtilleryArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllyArtilleryArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllyArtilleryArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EnemyArtilleryArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyArtilleryArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnemyArtilleryArray;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ball_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Ball;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BallClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_BallClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BallArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BallArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BallArray;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DecalMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DecalMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowMouseCursor_MetaData[];
#endif
		static void NewProp_bShowMouseCursor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowMouseCursor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AArcanoidPawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_MyProject11,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AArcanoidPawn_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AArcanoidPawn_DecalMove, "DecalMove" }, // 2896011811
		{ &Z_Construct_UFunction_AArcanoidPawn_HorisontalInputReaction, "HorisontalInputReaction" }, // 1043112027
		{ &Z_Construct_UFunction_AArcanoidPawn_SetCameraLocationAndRotation, "SetCameraLocationAndRotation" }, // 170958113
		{ &Z_Construct_UFunction_AArcanoidPawn_SpawnArtillery, "SpawnArtillery" }, // 772397126
		{ &Z_Construct_UFunction_AArcanoidPawn_SpawnRacket, "SpawnRacket" }, // 75615812
		{ &Z_Construct_UFunction_AArcanoidPawn_SpawnTank, "SpawnTank" }, // 1949112670
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "ArcanoidPawn.h" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Camera_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Camera = { "Camera", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, Camera), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Camera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Camera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Racket_MetaData[] = {
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Racket = { "Racket", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, Racket), Z_Construct_UClass_ACamera_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Racket_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Racket_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_RacketClass_MetaData[] = {
		{ "Category", "ArcanoidPawn" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_RacketClass = { "RacketClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, RacketClass), Z_Construct_UClass_ACamera_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_RacketClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_RacketClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Tank_MetaData[] = {
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Tank = { "Tank", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, Tank), Z_Construct_UClass_ATank_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Tank_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Tank_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_TankClass_MetaData[] = {
		{ "Category", "ArcanoidPawn" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_TankClass = { "TankClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, TankClass), Z_Construct_UClass_ATank_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_TankClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_TankClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_ArtilleryClass_MetaData[] = {
		{ "Category", "ArcanoidPawn" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_ArtilleryClass = { "ArtilleryClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, ArtilleryClass), Z_Construct_UClass_ATank_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_ArtilleryClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_ArtilleryClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyTankArray_Inner = { "AllyTankArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ATank_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyTankArray_MetaData[] = {
		{ "Category", "ArcanoidPawn" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyTankArray = { "AllyTankArray", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, AllyTankArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyTankArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyTankArray_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyTankArray_Inner = { "EnemyTankArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ATank_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyTankArray_MetaData[] = {
		{ "Category", "ArcanoidPawn" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyTankArray = { "EnemyTankArray", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, EnemyTankArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyTankArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyTankArray_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyArtilleryArray_Inner = { "AllyArtilleryArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ATank_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyArtilleryArray_MetaData[] = {
		{ "Category", "ArcanoidPawn" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyArtilleryArray = { "AllyArtilleryArray", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, AllyArtilleryArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyArtilleryArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyArtilleryArray_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyArtilleryArray_Inner = { "EnemyArtilleryArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ATank_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyArtilleryArray_MetaData[] = {
		{ "Category", "ArcanoidPawn" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyArtilleryArray = { "EnemyArtilleryArray", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, EnemyArtilleryArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyArtilleryArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyArtilleryArray_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Ball_MetaData[] = {
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Ball = { "Ball", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, Ball), Z_Construct_UClass_ABall_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Ball_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Ball_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallClass_MetaData[] = {
		{ "Category", "ArcanoidPawn" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallClass = { "BallClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, BallClass), Z_Construct_UClass_ABall_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallArray_Inner = { "BallArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ABall_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallArray_MetaData[] = {
		{ "Category", "ArcanoidPawn" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallArray = { "BallArray", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, BallArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallArray_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_DecalMaterial_MetaData[] = {
		{ "Category", "Cursor" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_DecalMaterial = { "DecalMaterial", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AArcanoidPawn, DecalMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_DecalMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_DecalMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_bShowMouseCursor_MetaData[] = {
		{ "Category", "MouseInterface" },
		{ "ModuleRelativePath", "ArcanoidPawn.h" },
	};
#endif
	void Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_bShowMouseCursor_SetBit(void* Obj)
	{
		((AArcanoidPawn*)Obj)->bShowMouseCursor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_bShowMouseCursor = { "bShowMouseCursor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(AArcanoidPawn), &Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_bShowMouseCursor_SetBit, METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_bShowMouseCursor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_bShowMouseCursor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AArcanoidPawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Camera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Racket,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_RacketClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Tank,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_TankClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_ArtilleryClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyTankArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyTankArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyTankArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyTankArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyArtilleryArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_AllyArtilleryArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyArtilleryArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_EnemyArtilleryArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_Ball,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_BallArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_DecalMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AArcanoidPawn_Statics::NewProp_bShowMouseCursor,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AArcanoidPawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AArcanoidPawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AArcanoidPawn_Statics::ClassParams = {
		&AArcanoidPawn::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AArcanoidPawn_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AArcanoidPawn_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AArcanoidPawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AArcanoidPawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AArcanoidPawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AArcanoidPawn, 190398404);
	template<> MYPROJECT11_API UClass* StaticClass<AArcanoidPawn>()
	{
		return AArcanoidPawn::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AArcanoidPawn(Z_Construct_UClass_AArcanoidPawn, &AArcanoidPawn::StaticClass, TEXT("/Script/MyProject11"), TEXT("AArcanoidPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AArcanoidPawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
