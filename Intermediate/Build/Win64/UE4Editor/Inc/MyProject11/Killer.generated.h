// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef MYPROJECT11_Killer_generated_h
#error "Killer.generated.h already included, missing '#pragma once' in Killer.h"
#endif
#define MYPROJECT11_Killer_generated_h

#define MyProject11_Source_MyProject11_Killer_h_15_SPARSE_DATA
#define MyProject11_Source_MyProject11_Killer_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execKill);


#define MyProject11_Source_MyProject11_Killer_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execKill);


#define MyProject11_Source_MyProject11_Killer_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAKiller(); \
	friend struct Z_Construct_UClass_AKiller_Statics; \
public: \
	DECLARE_CLASS(AKiller, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(AKiller)


#define MyProject11_Source_MyProject11_Killer_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAKiller(); \
	friend struct Z_Construct_UClass_AKiller_Statics; \
public: \
	DECLARE_CLASS(AKiller, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyProject11"), NO_API) \
	DECLARE_SERIALIZER(AKiller)


#define MyProject11_Source_MyProject11_Killer_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AKiller(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AKiller) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AKiller); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AKiller); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AKiller(AKiller&&); \
	NO_API AKiller(const AKiller&); \
public:


#define MyProject11_Source_MyProject11_Killer_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AKiller(AKiller&&); \
	NO_API AKiller(const AKiller&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AKiller); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AKiller); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AKiller)


#define MyProject11_Source_MyProject11_Killer_h_15_PRIVATE_PROPERTY_OFFSET
#define MyProject11_Source_MyProject11_Killer_h_12_PROLOG
#define MyProject11_Source_MyProject11_Killer_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_Killer_h_15_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_Killer_h_15_SPARSE_DATA \
	MyProject11_Source_MyProject11_Killer_h_15_RPC_WRAPPERS \
	MyProject11_Source_MyProject11_Killer_h_15_INCLASS \
	MyProject11_Source_MyProject11_Killer_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject11_Source_MyProject11_Killer_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject11_Source_MyProject11_Killer_h_15_PRIVATE_PROPERTY_OFFSET \
	MyProject11_Source_MyProject11_Killer_h_15_SPARSE_DATA \
	MyProject11_Source_MyProject11_Killer_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_Killer_h_15_INCLASS_NO_PURE_DECLS \
	MyProject11_Source_MyProject11_Killer_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYPROJECT11_API UClass* StaticClass<class AKiller>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject11_Source_MyProject11_Killer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
